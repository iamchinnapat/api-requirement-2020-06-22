const Joi = require('joi');
var express = require("express");
var cors = require("cors");
var mongoose = require("mongoose");
const bodyParser = require("body-parser");
const Users = require("./routers/usersrouter");
const Receipt = require("./routers/receiptrouter");
const Reward = require("./routers/rewardrouter");
const Transaction = require("./routers/transrouter");

var mongo_uri = "mongodb+srv://admin:VJwAPjeUBDt85xJ@iamchinnapat-zeop3.gcp.mongodb.net/api-requirement-2020-06-22?retryWrites=true&w=majority";
//admin VJwAPjeUBDt85xJ api-requirement-2020-06-22

mongoose.Promise = global.Promise;
mongoose.connect(mongo_uri, { 
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useNewUrlParser: true, })
.then (
  () => {console.log("[Success] : Connected to the Database ");},
  error => {
    console.log("[failed] " + error);
    process.exit();
  }
);

var app = express();
app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var port = process.env.PORT || 5555;

app.listen(port, () => {
  console.log("[Success] : Listening on Port " + port);
});

app.get("/", (req, res) => {
  res.status(200).send("Home");
});

app.use("/api/users", Users);
app.use("/api/receipt", Receipt);
app.use("/api/reward", Reward);
app.use("/api/trans", Transaction);

app.use((req, res, next) => {
  var err = new Error("not found");
  err.status = 404;
  next(err);
});