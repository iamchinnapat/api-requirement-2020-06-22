const Joi = require('joi');
var mongoose = require("mongoose");
var Users = require("../models/usersmodel");
var {Receipt} = require("../models/receiptmodel");

var transSchema = mongoose.Schema ( {
    userId: { type: String },
    productId: { type: String },
    usedscore: { type: Number },
  },
  {
    collection: "transaction"
  }
);

var Transaction = mongoose.model("transaction", transSchema);

function validateTransaction(transaction) {
  const schema = {
    userId: Joi.string().min(3).max(10).required(),    
    productId: Joi.string().min(1).max(9999).required(),
    usedscore: Joi.number().integer().min(0).max(9999).required(),
  };
  return Joi.validate(transaction, schema);
}

async function userUsedScore (username, result) {
  return await Transaction.aggregate([
      {$match: { userId: username }},
      {$group : { 
          _id: "$userId", 
          totalUsedScore : { $sum: "$usedscore"}
      }},
  ])
  .exec()
  .then(result => {
      let value = result[0].totalUsedScore
      return value
  })
}

async function userTotalScore (username, result) {
  return await Receipt.aggregate([
    {$match: { userId: username }},
    {$group : { 
      _id: "$userId", 
      totalTotalScore : { $sum: "$score"}
    }},
  ])
  .exec()
  .then(result => {
      let value = result[0].totalTotalScore
      return value
  })
}

exports.Transaction = Transaction;
exports.validate = validateTransaction;
exports.userusedscore = userUsedScore;
exports.usertotalscore = userTotalScore;