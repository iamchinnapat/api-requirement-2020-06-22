const Joi = require('joi');
const mongoose = require("mongoose");

const rewardSchema = mongoose.Schema ( {
    productID: { type: String },
    productName: { type: String },
    productPoint: { type: String }
  },
  {
    collection: "reward"
  }
);

const Reward = mongoose.model("reward", rewardSchema);

function validateReward(reward) {
  const schema = {
    productID: Joi.string().min(3).max(10).required(),
    productName: Joi.string().min(1).max(255).required(),
    productPoint: Joi.number().integer().min(0).max(9999).required()
  };
  return Joi.validate(reward, schema);
}

exports.Reward = Reward;
exports.validate = validateReward;