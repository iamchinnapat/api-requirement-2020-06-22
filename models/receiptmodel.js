const Joi = require('joi');
var mongoose = require("mongoose");

var receiptSchema = mongoose.Schema ( {
    userId: { type: String },
    receipt: { type: String },
    score: { type: Number },
    status: { type: Number }
  },
  {
    collection: "receipt"
  }
);

var Receipt = mongoose.model("receipt", receiptSchema);

function validateReceipt(receipt) {
  const schema = {
    userId: Joi.string().min(3).max(10).required(),    
    receipt: Joi.string().min(1).max(255).required(),
    score: Joi.number().integer().min(0).max(9999),
    status: Joi.number().integer().min(0).max(9999)
  };
  return Joi.validate(receipt, schema);
}

exports.Receipt = Receipt;
exports.validate = validateReceipt;