var mongoose = require("mongoose");

var usersSchema = mongoose.Schema ( 
  {
    userId: { 
      type: String 
    },
  },
  {
    collection: "users"
  }
);

var Users = mongoose.model("users", usersSchema);
module.exports = Users;