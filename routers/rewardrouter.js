var {Reward, validate} = require("../models/rewardmodel");
var express = require("express");
var router = express.Router();

router.get("/", (req, res) => {
  Reward.find().exec((err, data) => {
    if (err) return res.status(400).send(err);
    res.status(200).send(data);
  });
});

router.get("/:_id", (req, res) => {
  Reward.findById(req.params._id).exec((err, data) => {
    if (err) return res.status(400).send(err);
    res.status(200).send(data);
  });
});

router.post("/", async (req, res) => {
  const { error } = validate(req.body);
  console.log(error)
  if (error) {
    return res.status(400).send(error.details[0].message);
  }

  let productID = await Reward.findOne({ productID: req.body.productID });
  if (productID) {
    return res.status(400).send('รหัสสินค้าซ้ำ');
  } else {
    var obj = new Reward(req.body);
    await obj.save((err, data) => {
      if (err) return res.status(400).send(err);
      res.status(200).send("เพิ่มข้อมูลเรียบร้อย");
    });
  }
});

router.put("/:_id", (req, res) => {
  Reward.findByIdAndUpdate(req.params._id, req.body, (err, data) => {
    if (err) return res.status(400).send(err);
    res.status(200).send("อัพเดทข้อมูลเรียบร้อย");
  });
});

router.delete("/:_id", async (req, res) => {
  let id = await Reward.findOne({ _id: req.params._id });
  // console.log(id)
  if (id != null) {
    Reward.findByIdAndDelete(req.params._id, (err, data) => {
      if (err) return res.status(400).send(err);
      res.status(200).send("ลบข้อมูลเรียบร้อย");
    });
  } else {return res.status(400).send('ไม่พบข้อมูล');}
});

module.exports = router;