var {Receipt, validate} = require("../models/receiptmodel");
// var Transaction = require("../models/transmodel");
var Users = require("../models/usersmodel");
var express = require("express");
var router = express.Router();

router.get("/", (req, res) => {
  Receipt.find().exec((err, data) => {
    if (err) return res.status(400).send(err);
    res.status(200).send(data);
  });
});

router.get("/totalscore", (req, res) => {
  Receipt
  .aggregate([
    {$group : { 
      _id: "$userId", 
      totalScore : { $sum: "$score"}
    }},
  ])
  
  .exec((err, data) => {
    if (err) return res.status(400).send(err);
    res.status(200).send(data);
  });
});

router.get("/totalscore/:userId", (req, res) => {
  Receipt
  .aggregate([
    {$match: { userId: req.params.userId }},
    {$group : { 
      _id: "$userId", 
      totalScore : { $sum: "$score"}
    }},
  ])
  
  .exec((err, data) => {
    if (err) return res.status(400).send(err);
    res.status(200).send(data);
  });
});

router.post("/", async (req, res) => {
  let user = await Users.findOne({ userId: req.body.userId });
  if (user) {
    const { error } = validate(req.body);
    if (error) {
      return res.status(400).send(error.details[0].message);
    }
  
    var obj = new Receipt(req.body);
    obj.save((err, data) => {
      if (err) return res.status(400).send(err);
      res.status(200).send("เพิ่มข้อมูลเรียบร้อย");
    });
  } else {return res.status(400).send('ไม่พบข้อมูลผู้ใช้งาน');}
});

router.put("/:_id", (req, res) => {
  if (req.body.score >= 0) {
    Receipt.findByIdAndUpdate(req.params._id, req.body, (err, data) => {
      if (err) return res.status(400).send(err);
      res.status(200).send("อัพเดทข้อมูลเรียบร้อย");
    });
  } else {return res.status(400).send('ไม่สามารถให้คะแนนน้อยกว่า 0 ได้');}
});

router.delete("/:_id", async (req, res) => {
  let id = await Receipt.findOne({ _id: req.params._id });
  if (id != null) {
    Receipt.findByIdAndDelete(req.params._id, (err, data) => {
      if (err) return res.status(400).send(err);
      res.status(200).send("ลบข้อมูลเรียบร้อย");
    });
  } else {return res.status(400).send('ไม่พบข้อมูล');}
});

module.exports = router;