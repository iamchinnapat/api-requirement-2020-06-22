var {Transaction, validate, userusedscore, usertotalscore} = require("../models/transmodel");
var Users = require("../models/usersmodel");
var {Reward} = require("../models/rewardmodel");
var {Receipt} = require("../models/receiptmodel");
var express = require("express");
var router = express.Router();

    router.get("/", (req, res) => {
        Transaction.find().exec((err, data) => {
        if (err) return res.status(400).send(err);
        res.status(200).send(data);
        });
    });
  
    router.get("/:userId", (req, res) => {
        Transaction
        .find(
            { userId: req.params.userId }
        )
        .exec((err, data) => {
            if (err) return res.status(400).send(err);
            res.status(200).send(data);
        });
    });

    router.get("/totalscore", (req, res) => {
        Transaction
        .aggregate([
          {$group : { 
            _id: "$userId", 
            totalUsedScore : { $sum: "$usedscore"}
          }},
        ])
        .exec((err, data) => {
          if (err) return res.status(400).send(err);
          res.status(200).send(data);
        });
      });
      
    router.get("/totalscore/:userId", (req, res) => {
        Transaction
        .aggregate([
            {$match: { userId: req.params.userId }},
            {$group : { 
                _id: "$userId", 
                totalUsedScore : { $sum: "$usedscore"}
            }},
        ])
        .exec((err, data) => {
            if (err) return res.status(400).send(err);
            res.status(200).send(data);
        });
    });

    // router.get("/remainScore/:userId", async (req, res) => {
    //     let user = await Users.findOne({ userId: req.params.userId });
    //     if (user) {
    //         let usedscore = await userusedscore(req.params.userId)
    //         let totalscore = await usertotalscore(req.params.userId)
    //         if(totalscore < usedscore) {return res.status(400).send('คะแนนไม่พอ');}
    //         else {

                
    //         }
    //     } else {return res.status(400).send('กรุณาตรวจสอบ userId');}
    // });

    router.post("/", async (req, res) => {
        let user = await Users.findOne({ userId: req.body.userId });
        let reward = await Reward.findOne({ productID: req.body.productId });
        if ((user) && (reward)) {
            let total = await Transaction.findOne({ userId: req.body.userId });
            console.log(total)
            if (total == null) {usedscore = 0} else {usedscore = await userusedscore(req.body.userId)}
            let totalscore = await usertotalscore(req.body.userId)
            if(totalscore <= usedscore) {return res.status(400).send('คะแนนไม่พอ');}
            else {
                const { error } = validate(req.body);
                if (error) {return res.status(400).send(error.details[0].message);}
                var obj = new Transaction(req.body);
                obj.save((err, data) => {
                    if (err) return res.status(400).send(err);
                    res.status(200).send("เพิ่มข้อมูลเรียบร้อย");
                });
            }
        } else {return res.status(400).send('กรุณาตรวจสอบ userId และ productId');}
    });
  
    router.put("/:_id", (req, res) => {
        if (req.body.score >= 0) {
            Transaction.findByIdAndUpdate(req.params._id, req.body, (err, data) => {
            if (err) return res.status(400).send(err);
            res.status(200).send("อัพเดทข้อมูลเรียบร้อย");
        });
        } else {return res.status(400).send('ไม่สามารถให้คะแนนน้อยกว่า 0 ได้');}
    });
  
    router.delete("/:_id", async (req, res) => {
        let id = await Transaction.findOne({ _id: req.params._id });
        if (id != null) {
            Transaction.findByIdAndDelete(req.params._id, (err, data) => {
            if (err) return res.status(400).send(err);
            res.status(200).send("ลบข้อมูลเรียบร้อย");
        });
        } else {return res.status(400).send('ไม่พบข้อมูล');}
    });
  
  module.exports = router;