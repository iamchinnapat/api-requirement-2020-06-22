var express = require("express");
var router = express.Router();
var Users = require("../models/usersmodel");

router.get("/", (req, res) => {
  Users.find().exec((err, data) => {
    if (err) return res.status(400).send(err);
    res.status(200).send(data);
  });
});

// router.get("/:_id", (req, res) => {
//   Users.findById(req.params._id).exec((err, data) => {
//     if (err) return res.status(400).send(err);
//     res.status(200).send(data);
//   });
// });

router.post("/", async (req, res) => {
  let user = await Users.findOne({ userId: req.body.userId });
  if (user) {
    return res.status(400).send('ชื่อผู้ใช้งานซ้ำ');
  } else {
    var obj = new Users(req.body);
    await obj.save((err, data) => {
      if (err) return res.status(400).send(err);
      res.status(200).send("เพิ่มข้อมูลเรียบร้อย");
    });
  }
});

router.put("/:_id", (req, res) => {
  Users.findByIdAndUpdate(req.params._id, req.body, (err, data) => {
    if (err) return res.status(400).send(err);
    res.status(200).send("อัพเดทข้อมูลเรียบร้อย");
  });
});

router.delete("/:_id", (req, res) => {
  Users.findByIdAndDelete(req.params._id, (err, data) => {
    if (err) return res.status(400).send(err);
    res.status(200).send("ลบข้อมูลเรียบร้อย");
  });
});

module.exports = router;